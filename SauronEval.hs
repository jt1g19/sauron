module SauronEval where
import SauronGrammar
import Data.List
import Control.Exception
import System.IO
import Data.Char
import System.Directory

evalAll :: Program -> IO()
evalAll (Export expr f) = do  t <- eval expr
                              printTable t
                              outputCSV t f
evalAll (Expr e) =        do  t <- eval e
                              printTable t

printTable :: Table -> IO()
printTable t = mapM_ putStrLn $ map concat $ map (\r -> (map (++ ",") (init r)) ++ [last r]) t

eval :: SauronExp -> IO Table
eval (Conjoin e) = conjoin e
eval (Read s) = open (s ++ ".csv")
eval (Merge list) = merge list
eval (Sort expr) = do t <- eval expr
                      return (sort t)
eval (SortNum expr) = do t <- eval expr
                         return (sortNum t)
eval (Invert expr) = do t <- eval expr
                        return (reverse t)
eval (As expr col) = do t <- eval expr
                        return (as t col)
eval (Filter expr p) = do t <- eval expr
                          return (filterTable t p)
eval (JoinWith e1 e2) = do    t1 <- eval e1
                              t2 <- eval e2
                              return ([ x ++ y | x <- t1, y <-t2 ])

open :: String -> IO Table
open file = do    e <- doesFileExist file
                  if not e 
                        then error "The specified csv file to read the tables does not exist"
                        else pure()
                  s <- readFile (getName file)
                  let ls = lines (s)
                  let ss = map (splitOn ',') ls
                  let f = reverse . dropWhile isSpace
                  let ss' = map (map (f . f)) ss
                  return ss'

getName :: String -> String
getName ('$':xs) = xs
getName xs = xs

--it's from Julian's notes, there might be a library for this
splitOn :: Char -> String -> [String]
splitOn c [] = []
splitOn c ls = (takeWhile (/=c) ls) : splitOn' c (dropWhile (/=c) ls)
 where splitOn' c [] = []
       splitOn' c (x:[]) | x==c = [[]]
       splitOn' c (x:xs) | x==c = splitOn c xs
                         | otherwise = []

conjoin :: MergeList -> IO Table
conjoin (One x) = do    t <- eval x
                        return t
conjoin (Many x xs) = do      t1 <- eval x
                              t2 <- conjoin xs
                              return ([ x ++ y | x <- t1, y <-t2 ])

merge :: MergeList -> IO Table
merge (One x) = do      t <- eval x
                        return t
merge (Many x xs) = do  t1 <- eval x
                        t2 <- merge xs
                        return (join t1 t2)

join :: Table -> Table -> Table
join (x:xs) (y:ys) = (x ++ y):(join xs ys)
join _ _ = []

sortNum :: Table -> Table
sortNum = sortBy compare'

compare' :: Row -> Row -> Ordering
compare' [] [] = EQ
compare' [] _ = LT
compare' _ [] = GT
compare' (x:_) (y:_) | (not (isNumberX)) && (not (isNumberY)) = compare x y
                     | (isNumberX) && (not (isNumberY)) = LT
                     | (not (isNumberX)) && (isNumberY) = GT
                     | (read x :: Double) < (read y :: Double) = LT
                     | (read x :: Double) > (read y :: Double) = GT
                     | otherwise = EQ
                     where isNumberX = (all (\c -> isDigit c || c == '.') x) && ((length $ filter (=='.') x) <= 1)
                                           && (isDigit (head x)) && (isDigit (last x))
                           isNumberY = (all (\c -> isDigit c || c == '.') y) && ((length $ filter (=='.') y) <= 1)
                                           && (isDigit (head y)) && (isDigit (last y))

filterTable :: Table -> Predicate -> Table
filterTable (x:xs) p    | decide p x = x:(filterTable xs p)
                        | otherwise = filterTable xs p
filterTable [] _ = []

as :: Table -> ColumnList -> Table
as (x:xs) c = (mapRow x c):(as xs c)
as [] _ = []

mapRow :: Row -> ColumnList -> Row
mapRow row (C x) = (takeElem x row):[]
mapRow row (M x xs) = (takeElem x row):(mapRow row xs)

takeElem :: Column -> Row -> String
takeElem (S ('#':xs)) _ = xs
takeElem (I x) row      | x < length row = row !! x
                        | otherwise = error "IndexOutOfBounds"
takeElem (Cond p s1 s2) row   | decide p row = takeElem s1 row
                              | otherwise = takeElem s2 row
takeElem (F (Add c1 c2)) row = (takeElem c1 row) ++ (takeElem c2 row)
takeElem (F (Take x c)) row = take x (takeElem c row)
takeElem (F (Drop x c)) row = drop x (takeElem c row)

decide :: Predicate -> Row -> Bool
decide (Equal x y) row = takeElem x row == takeElem y row
decide (NotEqual x y) row = takeElem x row /= takeElem y row
decide (LessThan x y) row = takeElem x row < takeElem y row
decide (LessThanEq x y) row = takeElem x row <= takeElem y row
decide (GreaterThan x y) row = takeElem x row > takeElem y row
decide (GreaterThanEq x y) row = takeElem x row >= takeElem y row
decide (Conjunction p1 p2) row = decide p1 row && decide p2 row
decide (Disjunction p1 p2) row = decide p1 row || decide p2 row
 
outputCSV :: Table -> String -> IO()
outputCSV t f = do let css = intercalate "\n" (map (intercalate ",") (sort t))  
                   writeFile ((getName f) ++ ".csv") css
