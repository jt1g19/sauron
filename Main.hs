import SauronTokens
import SauronGrammar
import SauronEval
import System.Environment
import Control.Exception
import System.IO
import System.Directory

main :: IO ()
main = catch main' noParse

main' = do  (fileName : _ ) <- getArgs
            e <- doesFileExist fileName
            if not e 
                then error "The specified file to run the program does not exist"
                else pure()
            sourceText <- readFile fileName
            let lexedProg = alexScanTokens sourceText
            let parsedProg = parseSauron lexedProg
            evalAll parsedProg

noParse :: ErrorCall -> IO ()
noParse e = do let err =  show e
               hPutStr stderr err
               return ()
