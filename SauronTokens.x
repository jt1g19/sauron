{
module SauronTokens where
}

%wrapper "posn"
$filename = [A-Z]
$alpha = [a-zA-Z]
$digit = 0-9  

-- All the lexemes and their associated actions (i.e. conversion into tokens).
tokens :-
    $white+                         ;
    "--".*                          ;
    CONJOIN                         { \p s -> TokenConjoin p }
    JOIN_WITH                       { \p s -> TokenJoinWith p }
    FILTER                          { \p s -> TokenFilter p }
    WITH                            { \p s -> TokenWith p }
    AS                              { \p s -> TokenAs p }
    MERGE                           { \p s -> TokenMerge p }
    SORT                            { \p s -> TokenSort p }
    SORTNUM                         { \p s -> TokenSortNum p }
    INVERT                          { \p s -> TokenInvert p }
    TAKE                            { \p s -> TokenTake p }
    DROP                            { \p s -> TokenDrop p }
    \+                              { \p s -> TokenPlus p }
    \(                              { \p s -> TokenLParen p }
    \)                              { \p s -> TokenRParen p }
    \,                              { \p s -> TokenSeparate p }
    "=="                            { \p s -> TokenEq p }
    "!="                            { \p s -> TokenNotEq p }
    \<                              { \p s -> TokenLT p }
    "<="                            { \p s -> TokenLTEq p }
    \>                              { \p s -> TokenGT p }
    ">="                            { \p s -> TokenGTEq p }
    AND                             { \p s -> TokenAnd p }
    OR                              { \p s -> TokenOr p }
    IF                              { \p s -> TokenIf p }
    THEN                            { \p s -> TokenThen p }
    ELSE                            { \p s -> TokenElse p }
    EXPORT                          { \p s -> TokenExport p }
    $filename                       { \p s -> TokenFilename p s }
    $digit+                         { \p s -> TokenInt p (read s) }
    "#"    [$alpha $digit \_ \’]*   { \p s -> TokenString p s }
    "$"    [$alpha $digit \_]*      { \p s -> TokenFilename p s }

{
    
-- Data type for all the tokens in the Sauron language.
data SauronToken = 
    TokenConjoin AlexPosn             |
    TokenJoinWith AlexPosn            |
    TokenFilter AlexPosn              |
    TokenWith AlexPosn                |
    TokenAs AlexPosn                  |
    TokenMerge AlexPosn               |
    TokenSort AlexPosn                |
    TokenSortNum AlexPosn             | 
    TokenInvert AlexPosn              |
    TokenTake AlexPosn                |
    TokenDrop AlexPosn                |
    TokenPlus AlexPosn                |
    TokenLParen AlexPosn              |
    TokenRParen AlexPosn              |
    TokenSeparate AlexPosn            |
    TokenEq AlexPosn                  |
    TokenNotEq AlexPosn               |
    TokenLT AlexPosn                  |
    TokenLTEq AlexPosn                |
    TokenGT AlexPosn                  |
    TokenGTEq AlexPosn                |
    TokenAnd AlexPosn                 |
    TokenOr AlexPosn                  |
    TokenIf AlexPosn                  |
    TokenThen AlexPosn                |
    TokenElse AlexPosn                |
    TokenExport AlexPosn              |
    TokenFilename AlexPosn String     |
    TokenInt AlexPosn Int             | 
    TokenString AlexPosn String
    deriving (Eq, Show)

-- A function to return the (line:column) position of a token in the input file.
tokenPosn :: SauronToken -> String
tokenPosn (TokenConjoin (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenJoinWith (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenFilter (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenWith (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenAs (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenMerge (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenSort (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenSortNum (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenInvert (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenTake (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenDrop (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenPlus (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenLParen (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenRParen (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenSeparate (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenEq (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenNotEq (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenLT (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenLTEq (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenGT (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenGTEq (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenAnd (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenOr (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenIf (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenThen (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenElse (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenExport (AlexPn a l c)) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenFilename (AlexPn a l c) _) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenInt  (AlexPn a l c) _) = (show l) ++ ":" ++ (show c)
tokenPosn (TokenString (AlexPn a l c) _) = (show l) ++ ":" ++ (show c)

}
