{
module SauronGrammar where
import SauronTokens
}

%name parseSauron
%tokentype { SauronToken }
%error { parseError }
%token
    CONJOIN         { TokenConjoin _ }
    JOIN_WITH       { TokenJoinWith _ }
    FILTER          { TokenFilter _ }
    WITH            { TokenWith _ }
    AS              { TokenAs _ }
    MERGE           { TokenMerge _ }
    SORT            { TokenSort _ }
    SORTNUM         { TokenSortNum _ }
    INVERT          { TokenInvert _ }
    TAKE            { TokenTake _ }
    DROP            { TokenDrop _ }
    '+'             { TokenPlus _ }
    '('             { TokenLParen _ }
    ')'             { TokenRParen _ }
    ','             { TokenSeparate _ }
    "=="            { TokenEq _ }
    "!="            { TokenNotEq _ }
    '<'             { TokenLT _ }
    "<="            { TokenLTEq _ }
    '>'             { TokenGT _ }
    ">="            { TokenGTEq _ }
    AND             { TokenAnd _ }
    OR              { TokenOr _ }
    IF              { TokenIf _ }
    THEN            { TokenThen _ }
    ELSE            { TokenElse _ }
    EXPORT          { TokenExport _ }
    NAME            { TokenFilename _ $$ }
    INT             { TokenInt _ $$ }
    STRING          { TokenString _ $$ }

%nonassoc FILTER IF THEN ELSE NAME '(' ')' STRING INT "==" "!=" '<' "<=" '>' ">=" TAKE DROP
%left CONJOIN WITH AS MERGE SORT SORTNUM INVERT AND OR EXPORT JOIN_WITH '+' 
%%

Program : SauronExp EXPORT NAME                                     { Export $1 $3}
        | SauronExp                                                 { Expr $1 }

SauronExp : CONJOIN '(' MergeList ')'                               { Conjoin $3 }
          | FILTER SauronExp WITH Predicate                         { Filter $2 $4 }
          | SauronExp AS '(' ColumnList ')'                         { As $1 $4 }
          | MERGE '(' MergeList ')'                                 { Merge $3 }
          | SauronExp SORT                                          { Sort $1 }
          | SauronExp SORTNUM                                       { SortNum $1 }
          | INVERT '(' SauronExp ')'                                { Invert $3 }
          | '(' SauronExp ')'                                       { $2 }
          | SauronExp JOIN_WITH SauronExp                           { JoinWith $1 $3 }
          | NAME                                                    { Read $1 }

Predicate : Column "==" Column         { Equal $1 $3 }
          | Column "!=" Column         { NotEqual $1 $3 }
          | Column '<' Column          { LessThan $1 $3 }
          | Column "<=" Column         { LessThanEq $1 $3 }
          | Column '>' Column          { GreaterThan $1 $3 }
          | Column ">=" Column         { GreaterThanEq $1 $3 }
          | Predicate AND Predicate    { Conjunction $1 $3 }
          | Predicate OR Predicate     { Disjunction $1 $3 }
          | '(' Predicate ')'          { $2 }

Function    : Column '+' Column                             { Add $1 $3 }
            | TAKE INT Column                               { Take $2 $3 }
            | DROP INT Column                               { Drop $2 $3 }
            | '(' Function ')'                              { $2 }

Column     : STRING                                         { S $1 }
           | INT                                            { I $1 }
           | '(' IF Predicate THEN Column ELSE Column ')'   { Cond $3 $5 $7}
           | Function                                       { F $1 }

ColumnList : Column                                         { C $1}
           | Column ',' ColumnList                          { M $1 $3 }
           
MergeList : SauronExp              { One $1 }
          | SauronExp MergeList    { Many $1 $2 }

{

parseError :: [SauronToken] -> a
parseError [] = error "Unknown Parse Error" 
parseError (t:ts) = error ("Parse error at line:column " ++ (tokenPosn t))

data Program = Export SauronExp String | Expr SauronExp
    deriving (Show, Eq)

data SauronExp = Conjoin MergeList
               | Filter SauronExp Predicate
               | As SauronExp ColumnList
               | Merge MergeList
               | Sort SauronExp
               | SortNum SauronExp
               | Invert SauronExp
               | JoinWith SauronExp SauronExp
               | Read String
    deriving (Show, Eq)

type Table = [Row]

type Row = [String]

data Predicate = Equal Column Column
               | NotEqual Column Column
               | LessThan Column Column
               | LessThanEq Column Column
               | GreaterThan Column Column
               | GreaterThanEq Column Column
               | Conjunction Predicate Predicate
               | Disjunction Predicate Predicate
    deriving (Show, Eq)

data Function = Add Column Column | Take Int Column | Drop Int Column
    deriving (Show, Eq)

data Column = S String | I Int | Cond Predicate Column Column | F Function
    deriving (Show, Eq)

data ColumnList = C Column | M Column ColumnList
    deriving (Show, Eq)
    
data MergeList = One SauronExp | Many SauronExp MergeList
    deriving (Show, Eq)

    
}
